package com.example.watsaplicacion.adaptador

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.watsaplicacion.R
import com.example.watsaplicacion.estados.MostrarEstadosActivity
import com.example.watsaplicacion.mensajes.MensajesActivity
import com.mikhaellopez.circularimageview.CircularImageView


class AdaptadorEstados(list: List<listaEstados>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val list: List<listaEstados>

    private val VIEW_TYPE_MI_ESTADO = 1
    private val VIEW_TYPE_ESTADO_RECIENTE = 2
    private val VIEW_TYPE_ESTADO_VISTO = 3

    override fun getItemViewType(position: Int): Int {
        var estado: listaEstados = list.get(position)
        if (estado.getTipoVistaEstados() === 0) {
            Log.e("getItemViewType", "0")
            return VIEW_TYPE_MI_ESTADO
        } else if (estado.getTipoVistaEstados() === 1) {
            Log.e("getItemViewType", "1")
            return VIEW_TYPE_ESTADO_RECIENTE
        } else {
            Log.e("getItemViewType", "2")
            return VIEW_TYPE_ESTADO_VISTO
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_MI_ESTADO) {
            HolderEstados(
                LayoutInflater.from(parent?.context).inflate(
                    R.layout.lista_estados,
                    parent,
                    false
                )
            )
        } else if (viewType == VIEW_TYPE_ESTADO_RECIENTE) {
            HolderRecientes(
                LayoutInflater.from(parent?.context).inflate(
                    R.layout.lista_estados_recientes,
                    parent,
                    false
                )
            )
        } else {
            HolderVistos(
                LayoutInflater.from(parent?.context).inflate(
                    R.layout.lista_estados_vistos,
                    parent,
                    false
                )
            )
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val estados: listaEstados = list[position]
        when (holder.itemViewType) {
            VIEW_TYPE_MI_ESTADO -> (holder as HolderEstados)
            VIEW_TYPE_ESTADO_RECIENTE -> (holder as HolderRecientes).bind(estados)
            VIEW_TYPE_ESTADO_VISTO -> (holder as HolderVistos).bind(estados)
        }
        holder.itemView.setOnClickListener {
            var intent: Intent = Intent(holder.itemView.context, MostrarEstadosActivity::class.java)
            intent.putExtra("Estados", estados)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private class HolderEstados(itemview: View) : RecyclerView.ViewHolder(itemview) {
    }

    private class HolderRecientes(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val estadoReciente: TextView
        val horaReciente: TextView
        val perfilEstadoR: CircularImageView

        init {
            estadoReciente = itemview.findViewById(R.id.usuarioEstadoReciente)
            horaReciente = itemview.findViewById(R.id.fechaEstadoReciente)
            perfilEstadoR = itemview.findViewById(R.id.perfilEstadoReciente)
        }

        fun bind(estadoModel: listaEstados) {
            estadoReciente.setText(estadoModel.getNombreUserEstados())
            horaReciente.setText(estadoModel.getFechaUserEstados())
            estadoModel.getPerfilURLEstados()?.let { perfilEstadoR.setImageResource(it) }
        }
    }

    private class HolderVistos(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val estadoVisto: TextView
        val horaVisto: TextView
        val perfilEstadoV: CircularImageView

        init {
            estadoVisto = itemview.findViewById(R.id.usuarioEstadoVisto)
            horaVisto = itemview.findViewById(R.id.fechaEstadoVisto)
            perfilEstadoV = itemview.findViewById(R.id.perfilEstadoVisto)
        }

        fun bind(estadoModel: listaEstados) {
            estadoVisto.setText(estadoModel.getNombreUserEstados())
            horaVisto.setText(estadoModel.getFechaUserEstados())
            estadoModel.getPerfilURLEstados()?.let { perfilEstadoV.setImageResource(it) }
        }
    }


    init {
        this.list = list
    }

}