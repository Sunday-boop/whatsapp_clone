package com.example.watsaplicacion.mensajes

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watsaplicacion.R
import com.example.watsaplicacion.adaptador.AdaptadorMensajes
import com.example.watsaplicacion.adaptador.ListaChat
import com.example.watsaplicacion.adaptador.ListaMensaje
import com.mikhaellopez.circularimageview.CircularImageView
import kotlinx.android.synthetic.main.activity_mensajes.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MensajesActivity : AppCompatActivity() {

    private lateinit var imagenUsuarioM: CircularImageView
    private lateinit var nombreUsuarioM: TextView

    private lateinit var adapter1:AdaptadorMensajes
    private lateinit var listaM:ListaChat
    private var recyclerView: RecyclerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mensajes)
        setSupportActionBar(toolbarMensajes)
        atrasMensajes.setOnClickListener { onBackPressed() }


        initViews()
        initValues()
//      PONER MENSAJES
        val list = ArrayList<ListaMensaje>()
        recyclerView = findViewById(R.id.recyclerViewMensajes)
        val manager = LinearLayoutManager(this@MensajesActivity, RecyclerView.VERTICAL, false)
        recyclerView?.setLayoutManager(manager)

        list.add(ListaMensaje("kkwkw", "10:00 p.m.", 0))
        list.add(ListaMensaje("kkwkw", "12:21 p.m.", 1))


        recyclerView?.smoothScrollToPosition(list.size)
        adapter1 = AdaptadorMensajes(list)
        recyclerView?.adapter = adapter1

//        ENVIAR MENSAJE
        btnEnviarMensajes.setOnClickListener {

            var simpleDateFormat = SimpleDateFormat("h:mm a")
            var hora: String = simpleDateFormat.format(Date())
            

            val msg: String = editTextMensaje.getText().toString()
            list.add(ListaMensaje(msg, hora, 0))

            recyclerView?.smoothScrollToPosition(list.size)
            adapter1.notifyDataSetChanged()
            editTextMensaje.setText("")
        }

    }



    fun initViews(){

        imagenUsuarioM = findViewById(R.id.imagenUsuarioMensajes)
        nombreUsuarioM = findViewById(R.id.nombreUsuarioMensajes)
    }

    fun initValues(){

        listaM = (intent.extras?.getSerializable("Mensajes") as ListaChat?)!!
        nombreUsuarioM.setText(listaM.getNombreUser())
        listaM.getPerfilURL()?.let { imagenUsuarioM.setImageResource(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_mensajes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.ver_contacto -> Toast.makeText(
                this@MensajesActivity,
                "Ver contacto",
                Toast.LENGTH_LONG
            ).show()
            R.id.archivos_enlaces -> Toast.makeText(
                this@MensajesActivity,
                "Archivos, enlaces y docs",
                Toast.LENGTH_LONG
            )
                .show()
            R.id.buscar_mensajes -> Toast.makeText(
                this@MensajesActivity,
                "Buscar",
                Toast.LENGTH_LONG
            )
                .show()
            R.id.silenciar_not -> Toast.makeText(
                this@MensajesActivity,
                "Silenciar notificaciones",
                Toast.LENGTH_LONG
            )
                .show()
            R.id.fondo_pantalla -> Toast.makeText(
                this@MensajesActivity,
                "Fondo de pantalla",
                Toast.LENGTH_LONG
            )
                .show()
            R.id.mas -> Toast.makeText(this@MensajesActivity, "Mas", Toast.LENGTH_LONG)
                .show()
        }
        return super.onOptionsItemSelected(item)
    }
}



