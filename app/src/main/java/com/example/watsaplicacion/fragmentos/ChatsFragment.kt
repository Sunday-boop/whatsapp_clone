package com.example.watsaplicacion.fragmentos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watsaplicacion.R
import com.example.watsaplicacion.adaptador.*
import java.util.*
import kotlin.collections.ArrayList


class ChatsFragment : Fragment() {

    private var recyclerView: RecyclerView? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chats, container, false)

        val list = ArrayList<ListaChat>()
        recyclerView = view.findViewById(R.id.recyclerChats)
        recyclerView?.setLayoutManager(LinearLayoutManager(context))



        list.add(
            ListaChat(
                "11",
                "Leo",
                "Hola amigo",
                "Ayer",
                R.drawable.p2,
                "visto"
            )
        )
        list.add(
            ListaChat(
                "119",
                "Angelica",
                "Hellow",
                "hoy",
                R.drawable.p1,
                "noVisto"
            )
        )

        var adaptador = AdaptadorChats(list)
        recyclerView?.adapter = adaptador
        return  view
    }



}