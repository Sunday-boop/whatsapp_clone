 package com.example.watsaplicacion.adaptador

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.watsaplicacion.R
import com.example.watsaplicacion.mensajes.MensajesActivity
import com.mikhaellopez.circularimageview.CircularImageView


class AdaptadorChats(list: List<ListaChat>) : RecyclerView.Adapter<AdaptadorChats.Holder?>() {

    private val list: List<ListaChat>
//    private val context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.lista_chat, parent, false)
        return Holder(view)
    }
    override fun onBindViewHolder(holder: Holder, position: Int) {
        val listachat: ListaChat = list[position]
        holder.name.setText(listachat.getNombreUser())
        holder.mess.setText(listachat.getMensajeUser())
        holder.dia.setText(listachat.getFechaUser())

        if (listachat.getVistoUser().equals("visto")) {
            holder.visto.setImageResource(R.drawable.visto_azul)
        } else if (listachat.getVistoUser().equals("noVisto")){
            holder.visto.setImageResource(R.drawable.visto)
        }else{
            holder.visto.isInvisible
        }
        Glide.with(holder.perfil).load(listachat.getPerfilURL()).into(holder.perfil)

        holder.itemView.setOnClickListener {
            var intent: Intent= Intent(holder.itemView.context, MensajesActivity::class.java)
            intent.putExtra("Mensajes", listachat)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

     class Holder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val name: TextView
         val mess: TextView
         val dia: TextView
         val visto: ImageView
         val perfil: CircularImageView

        init {
            name = itemview.findViewById(R.id.usuarioChat)
            mess = itemview.findViewById(R.id.mensajeChat)
            dia = itemview.findViewById(R.id.fechaChat)
            perfil = itemview.findViewById(R.id.perfilChat)
            visto = itemview.findViewById(R.id.tipoVerificacion)
        }
    }

    init {
        this.list = list
    }
}

