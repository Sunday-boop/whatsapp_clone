package com.example.watsaplicacion.adaptador

class ListaMensaje {

    private var mensaje: String? = null
    private var horario: String? = null
    private var tipoVista: Int? = null

    constructor(mensaje: String?, horario: String?, tipoVista: Int?) {
        this.mensaje = mensaje
        this.horario = horario
        this.tipoVista = tipoVista
    }

    fun getMensaje(): String?{
        return mensaje
    }

    fun getHorario(): String?{
        return horario
    }

    fun getTipoVista(): Int?{
        return tipoVista
    }

}
